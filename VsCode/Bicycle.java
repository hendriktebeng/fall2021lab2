// Hendrik Tebeng - 2035508
public class Bicycle{
    String manufacturer;
    int numberGears;
    double maxSpeed;

    public Bicycle(String newManufacturer, int newNumberGears, double newMaxSpeed){
        this.manufacturer = newManufacturer;
        this.numberGears = newNumberGears;
        this.maxSpeed = newMaxSpeed;
    }

    public String getManufacturer(){
        return this.manufacturer;
    }
    public int getNumberGears(){
        return this.numberGears;
    }
    public double getMaxSpeed(){
        return this.maxSpeed;
    }

    public String toString(){
        return "Manufacturer: " + this.manufacturer + ", Number of Gears: " + this.numberGears + ", Max Speed: " + this.maxSpeed;
    }
}