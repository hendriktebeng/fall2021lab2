// Hendrik Tebeng - 2035508
public class BikeStore{
    public static void main(String[] agrs){
        Bicycle[] bicycles = new Bicycle[4];
        bicycles[0] = new Bicycle("Hendrik", 3, 18.7);
        bicycles[1] = new Bicycle("Dan", 5, 19.2);
        bicycles[2] = new Bicycle("Andrew", 7, 13.5);
        bicycles[3] = new Bicycle("Jackie", 5, 15.9);
    

        for(Bicycle item : bicycles){
            System.out.println(item);
        }
    }
}
